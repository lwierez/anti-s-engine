#ifndef PROTECT_SAMPLE
#define PROTECT_SAMPLE

/*
 * Sample function.
 * It basically does nothing, but illustrates how function or class should be
 * written and tested.
 * The prototype of the function is declared in this file, the definition of
 * this function is in /src/sample.cpp.
 * The function is tested in /test/sample.cpp.
 */
double add(double, double);

#endif
