# Anti's Engine

A video game engine written in C++ with the [SFML API](https://www.sfml-dev.org/).

## Philosophy

The goal of the Anti's Engine is to provide a multi-plateform engine for development with a minimal UI and constraint. The engine with made for developers who want to keep a control on the code in C++.
