#include <catch2/catch_test_macros.hpp>

#include "../include/sample.h"


TEST_CASE( "Test of a sample function", "[sample]" )
{
    REQUIRE( add(1, 1) == 2 );
}
